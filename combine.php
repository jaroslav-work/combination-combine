<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 30.11.16
 * Time: 0:04
 */


require_once 'CombinationCombine.php';
$opt = getopt('k:n:');
$i = 0;
file_put_contents('log.txt', "");
file_put_contents('log.txt', "Combinations count <10 ".PHP_EOL);
print total($opt['k'], $opt['n']);
foreach (CombinationCombine::FetchCombinations($opt['k'], $opt['n']) as $num) {
    $i++;
    $bin_str = str_pad(decbin($num), $opt['n'], '0', STR_PAD_LEFT);
    file_put_contents('log.txt', $bin_str.PHP_EOL, FILE_APPEND);
    if ($i > 10) {
        exec("sed -i '1s/.*/Combinations count $i/' log.txt");
    }
}

function total($k, $n)
{
    $t = 1;
    for ($i = $n, $c = $n - ($k - 1); $i >= $c; $i--) $t *= $i;
    for ($i = $k; $i >= 1; $i--) $t /= $i;
    return $t;
}

