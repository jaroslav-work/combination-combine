<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 23.11.16
 * Time: 22:21
 */

/**
 * Побитовое И (AND)
 * ( 0 = 0000) = ( 0 = 0000) & ( 5 = 0101)
 * ( 1 = 0001) = ( 1 = 0001) & ( 5 = 0101)
 * ( 0 = 0000) = ( 2 = 0010) & ( 5 = 0101)
 * ( 4 = 0100) = ( 4 = 0100) & ( 5 = 0101)
 * ( 0 = 0000) = ( 8 = 1000) & ( 5 = 0101)
 *Побитовое (включающее) ИЛИ (OR)
 * ( 5 = 0101) = ( 0 = 0000) | ( 5 = 0101)
 * ( 5 = 0101) = ( 1 = 0001) | ( 5 = 0101)
 * ( 7 = 0111) = ( 2 = 0010) | ( 5 = 0101)
 * ( 5 = 0101) = ( 4 = 0100) | ( 5 = 0101)
 * (13 = 1101) = ( 8 = 1000) | ( 5 = 0101)
 *
 *  Побитовое ИСКЛЮЧАЮЩЕЕ ИЛИ (XOR)
 * ( 5 = 0101) = ( 0 = 0000) ^ ( 5 = 0101)
 * ( 4 = 0100) = ( 1 = 0001) ^ ( 5 = 0101)
 * ( 7 = 0111) = ( 2 = 0010) ^ ( 5 = 0101)
 * ( 1 = 0001) = ( 4 = 0100) ^ ( 5 = 0101)
 * (13 = 1101) = ( 8 = 1000) ^ ( 5 = 0101)
 */
class CombinationCombine
{

    //Генерация первой комбинации
    public static function getFirstIntCombination($k, $n)
    {
        return ( ((1 << $k) - 1) << ($n - $k) );

    }

    public static function unitStep($int_combination)
    {
        return (($int_combination - 1) ^ ((($int_combination - 1) ^ $int_combination) >> 2)); // 11000 => 10100
    }

    //10000 => 11000
    public static function pushUnit($int_combination)
    {
        return ((($int_combination ^ ($int_combination - 1)) + 1) >> 2) | $int_combination;
    }


    //Следующая
    /**
     * @param $int_combination
     * @return int
     */
    public static function recursionNextIntCombination($int_combination)
    {
        //15 & 16 = 0; 16 & 17 = 16
        if (($int_combination & ($int_combination + 1)) == 0) return 0;

        /*
         *  php > print 81 & 1; >> 1 ()
         *  php > print 81 & 82; >> 80 (1010001 & 1010010 = 1010000)
         * */

        if ($int_combination & 1) {
            return self::pushUnit(self::recursionNextIntCombination($int_combination >> 1) << 1);
        }


        return self::unitStep($int_combination);
    }



    public static function FetchCombinations($k, $n)
    {
        $comb = self::getFirstIntCombination($k, $n);
        do {
            yield $comb;
        } while ($comb = self::recursionNextIntCombination($comb));
        
        return 0;
    }

}

